import * as cdk from '@aws-cdk/core';
import * as s3 from '@aws-cdk/aws-s3';
import * as s3deploy from '@aws-cdk/aws-s3-deployment';
import { exec } from 'child_process';

export class CdktestStack extends cdk.Stack {
  constructor(scope: cdk.Construct, id: string, props?: cdk.StackProps) {
    super(scope, id, props);

    exec("npm --prefix ./vue-boilerplate run build");

    const bucket = new s3.Bucket(this, 'testbucket', {
      websiteIndexDocument: 'index.html',
      publicReadAccess: true
    });

    new s3deploy.BucketDeployment(this, 'DeployVueBoilerplate', {
      sources: [s3deploy.Source.asset('./vue-boilerplate/dist')],
      destinationBucket: bucket
    });
  }
}
